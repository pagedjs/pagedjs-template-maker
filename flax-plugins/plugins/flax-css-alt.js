/*This plugins convert and export your css*/
/* Flax uses [ LightningCss ](https://lightningcss.dev/docs.html) to genereate the content from the "src" folder. */
/* this is a patched version (to include incremental rebuilds) from https://github.com/5t3ph/eleventy-plugin-lightningcss */

/*  WARNING THIS IS MOVING CSS AROUND, it’s an alt for template*/

// experimenting some new css way with lightningcss
const matter = require("gray-matter");
const fs = require("node:fs");
const path = require("node:path");
const browserslist = require("browserslist");
const {
  bundle,
  browserslistToTargets,
  composeVisitors,
} = require("lightningcss");

// Set default transpiling targets
let browserslistTargets = "> 0.2% and not dead";

// Check for user's browserslist
try {
  const package = path.resolve(__dirname, fs.realpathSync("package.json"));
  const userPkgBrowserslist = require(package);

  if (userPkgBrowserslist.browserslist) {
    browserslistTargets = userPkgBrowserslist.browserslist;
  } else {
    try {
      const browserslistrc = path.resolve(
        __dirname,
        fs.realpathSync(".browserslistrc"),
      );

      fs.readFile(browserslistrc, "utf8", (_err, data) => {
        if (data.length) {
          browserslistTargets = [];
        }

        data.split(/\r?\n/).forEach((line) => {
          if (line.length && !line.startsWith("#")) {
            browserslistTargets.push(line);
          }
        });
      });
    } catch (err) {
      // no .browserslistrc
    }
  }
} catch (err) {
  // no package browserslist
}

module.exports = (eleventyConfig, options) => {
  const defaults = {
    importPrefix: "_",
    nesting: true,
    customMedia: true,
    minify: true,
    sourceMap: false,
    visitors: [
      // change font url?

      {
        FontFace(loc, properties) {
          console.log(lo.properties);
        },
      },
    ],
    customAtRules: {},
  };

  const {
    importPrefix,
    nesting,
    customMedia,
    minify,
    sourceMap,
    visitors,
    customAtRules,
  } = {
    ...defaults,
    ...options,
  };

  // Recognize CSS as a "template language"
  eleventyConfig.addTemplateFormats("css");

  // Process CSS with LightningCSS
  eleventyConfig.addExtension("css", {
    outputFileExtension: "css",
    compile: async function(_inputContent, inputPath) {
      let parsed = path.parse(inputPath);
      if (parsed.name.startsWith(importPrefix)) {
        return;
      }

      // Support @import triggering regeneration for incremental builds
      // h/t @julientaq for the fix
      if (_inputContent.includes("@import")) {
        // for each file create a list of files to look at
        const fileList = [];

        // get a list of import on the file your reading
        const importRuleRegex =
          /@import\s+(?:url\()?['"]?(?!http)([^'"\);]+)['"]?\)?.*;/g;

        let match;
        while ((match = importRuleRegex.exec(_inputContent))) {
          fileList.push(parsed.dir + "/" + match[1]);
        }

        this.addDependencies(inputPath, fileList);
      }

      //
      let targets = browserslistToTargets(browserslist(browserslistTargets));

      // output
      // const output = inputPath;

      return async () => {
        //move all files to the /css folder so we don’t care about the location of the files
        let { code } = bundle({
          filename: inputPath,
          code: Buffer.from(_inputContent),
          minify,
          sourceMap,
          targets,
          drafts: {
            nesting,
            customMedia,
          },
          customAtRules,
          visitor: composeVisitors(visitors),
        });

        // change the font url

        let newCSS = "" + code;

        if (inputPath.includes("/templates/")) {
          /* the fonts are linked */
          /*css fonts change the url to the fonts to /fonts/* */
          const newFontPath = "/fonts/"; // Replace with the new font file path

          // Define a regular expression to find src properties with font file references
          const srcPropertyRegex =
            /src:\s?.*?["']?([^/]*)(\.eot|\.woff2|\.woff|\.ttf|\.otf)["']?\)/g;

          // Use replace() with a callback function to modify the paths in the CSS string
          const updatedCSS = newCSS.replace(
            srcPropertyRegex,
            (match, fontName, fontExtension) => {
              return `src: url("/fonts/${fontName}${fontExtension}")`;
            },
          );

          newCSS = `@charset "utf-8";\n` + updatedCSS;
        }

        if (inputPath.includes("templates/")) {
          //check if inputPath exist
          var dir = "./public/css/templates";

          if (!fs.existsSync(dir)) {
            fs.mkdirSync(dir, { recursive: true });
          }

          fs.writeFile(
            `./public/css/templates/${inputPath.split("/")[inputPath.split("/").length - 1]
            }`,
            newCSS.length > 1 ? newCSS : `@charset "utf-8";\n` + code,
            { encoding: "utf8" },

            (err) => {
              if (err) console.log(err);
              else {
                console.log(
                  `File ${inputPath.split("/")[inputPath.split("/").length - 1]
                  } written successfully`,
                );
              }
            },
          );
        }
        return code;
      };

      /*let create a css file in the css folder */
    },
  });
};
