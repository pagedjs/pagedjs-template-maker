// multiple needed filters
//
module.exports = function(eleventyConfig) {
  eleventyConfig.addFilter("not", function(array, key, value) {
    return array.filter((el) => {
      return el[key] != value;
    });
  });
  eleventyConfig.addFilter("with", function(array, key, value) {
    return array.filter((el) => {
      return el[key] == value;
    });
  });
  eleventyConfig.addFilter("notcontains", function(array, key, value) {
    return array.filter((el) => {
      return !el[key].includes(value);
    });
  });
  eleventyConfig.addFilter("contains", function(array, key, value) {
    return array.filter((el) => {
      return el[key].includes(value);
    });
  });
};
