const Image = require("@11ty/eleventy-img");
const fs = require("fs");

// Only one module.exports per configuration file, please!
module.exports = function (eleventyConfig) {
  eleventyConfig.addShortcode("flaxImage", async function (src, alt) {
    //check if the url is external
    let imgSrcUrl = !src.startsWith("http") ? `./static/images/${src}` : src;

    let metadata = await Image(imgSrcUrl, {
      widths: [600],
      formats: ["jpeg"],
      outputDir: "./public/images",
      urlPath: "/images/",
    });

    let data = metadata.jpeg[metadata.jpeg.length - 1];
    return `<img src="${data.url}" width="${data.width}" height="${data.height}" alt="${alt}" loading="lazy" decoding="async">`;
  });
};


