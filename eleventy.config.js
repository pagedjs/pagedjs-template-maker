const { EleventyHtmlBasePlugin } = require("@11ty/eleventy");

const markdownIt = require("markdown-it");
const markdownItPandoc = require("markdown-it-pandoc");

const flax = require("./flax-plugins/flax.js");

module.exports = function(eleventyConfig) {
  // add base html to every url
  eleventyConfig.addPlugin(EleventyHtmlBasePlugin);

  eleventyConfig.addPlugin(flax);

  // set collection of stylesheets
  //
  eleventyConfig.addCollection("dummy", (collectionApi) => {
    // create a collection for everything that need to be in the menu, using the menutitle metadata

    return collectionApi.getFilteredByGlob("./src/content/dummy/**/*.html");
  });

  eleventyConfig.addCollection("templates", (collectionApi) => {
    // create a collection for everything that need to be in the menu, using the menutitle metadata

    return collectionApi
      .getFilteredByGlob("./src/content/templates/**/*.md")
      .sort(function(a, b) {
        return b.title - a.title;
      });
  });

  let options = {
    html: true, // Enable HTML tags in source
    linkify: true,
    breaks: true,
  };

  // configure the library with options
  let md = markdownIt(options).use(markdownItPandoc);

  eleventyConfig.setLibrary("md", markdownIt(options));

  /*copy all font found in the /fonts folders */

  eleventyConfig.addPassthroughCopy({
    "./src/content/**/*.ttf": "/fonts",
    "./src/content/**/*.woff": "/fonts",
    "./src/content/**/*.woff2": "/fonts",
    "./src/content/**/*.otf": "/fonts",
    "./src/content/**/*.eot": "/fonts",
  });

  eleventyConfig.addPassthroughCopy({
    "./src/content/**/*.jpg": "/images",
    "./src/content/**/*.jpeg": "/images",
    "./src/content/**/*.png": "/images",
    "./src/content/**/*.svg": "/images",
    "./src/content/**/*.webp": "/images",
  });
  /*party js*/
  eleventyConfig.addPassthroughCopy({
    "./static/js": "/js/",
  });

  return {
    // templates engines : njk
    markdownTemplateEngine: "njk",

    // set the directories
    dir: {
      input: "src",
      output: "public",
      includes: "layouts",
      data: "data",
    },
  };
};
