// add to kotahi output and not to pagedjs

class qrcodingDoi extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
  }

  afterRendered() {
    let qr = document.querySelector(".qrcode");
    if (!qr) return

    // The URL you want to encode in the QR code
    const urlToEncode = qr.textContent;
    if (!urlToEncode) return;

    // remove the textContent
    qr.textContent = "";

    // Create a new QRCode object and pass in the <div> element
    const qrcode = new QRCode({
      content: urlToEncode,
      width: 80,
      height: 80,
      padding: 0,
      ecl: "L",
      color: "#001360",
      background: "#ffffff",
      pretty: false,
      xmlDeclaration: false,
    });
    qr.innerHTML = qrcode.svg();
  }
}

Paged.registerHandlers(qrcodingDoi);
