// Define here the tags you want to give id

// this is quite a hack
// Set the handler
class text2link extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
  }

  beforeParsed(content) {
    const template = document.querySelector("template");
    template.innerHTML = template.innerHTML.replace(
      /(?<!src=|href=|[A-])["']?(https?:\/\/\S+)/gi,
      (match, url) => {
        return `<a class="pagedjs-added-link" href="${url}">${url}</a>`;
      },
    );
  }
}
Paged.registerHandlers(text2link);
