---
title: all templates
permalink: "/templates/{{ template.data.title | slugify }}/{{template.data.booksize | slugify}}/"
layout: templates/template-single.njk
pagination:
  data: collections.templates
  size: 1
  alias: template
---
